package br.edu.unisep.store.controller;

import br.edu.unisep.store.domain.dto.ProductsDto;
import br.edu.unisep.store.domain.dto.RegisterProductsDto;
import br.edu.unisep.store.domain.usecase.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductsController {

    @GetMapping
    public ResponseEntity<List<ProductsDto>> findAll() {
        var useCase = new FindAllProductsUseCase();
        var result = useCase.execute();

        if(result.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Integer id) {
        var useCase = new FindProductsByIdUseCase();

        try {
            var products = useCase.execute(id);

            if (products == null) {
                return ResponseEntity.notFound().build();
            }

            return ResponseEntity.ok(products);
        }
        catch (IllegalArgumentException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody RegisterProductsDto products) {
        try {
            var useCase = new RegisterProductsUseCase();
            useCase.execute(products);

            return ResponseEntity.ok().build();
        } catch (IllegalArgumentException | NullPointerException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

}
