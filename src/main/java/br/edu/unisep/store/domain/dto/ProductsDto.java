package br.edu.unisep.store.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProductsDto {

    private final Integer id;

    private final String name;

    private final String description;

    private final Double value;

    private final String brand;

    private final String status;

}
