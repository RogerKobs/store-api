package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.ProductsDao;
import br.edu.unisep.store.data.entity.Products;
import br.edu.unisep.store.domain.dto.RegisterProductsDto;
import br.edu.unisep.store.domain.validator.ProductsValidator;

public class RegisterProductsUseCase {

    public void execute(RegisterProductsDto registerProducts) {
        var validator = new ProductsValidator();
        validator.validate(registerProducts);

        var products = new Products();
        products.setName(registerProducts.getName());
        products.setDescription(registerProducts.getDescription());
        products.setValue(registerProducts.getValue());
        products.setBrand(registerProducts.getBrand());
        products.setStatus(registerProducts.getStatus());

        var dao = new ProductsDao();
        dao.save(products);
    }

}
