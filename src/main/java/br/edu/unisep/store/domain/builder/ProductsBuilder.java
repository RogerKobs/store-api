package br.edu.unisep.store.domain.builder;

import br.edu.unisep.store.data.entity.Products;
import br.edu.unisep.store.domain.dto.ProductsDto;
import br.edu.unisep.store.domain.dto.RegisterProductsDto;

import java.util.List;
import java.util.stream.Collectors;

public class ProductsBuilder {

    public List<ProductsDto> from(List<Products> products) {
        return products.stream().map(this::from).collect(Collectors.toList());
    }

    public ProductsDto from(Products products) {
        if (products != null) {
            return new ProductsDto(
                    products.getId(),
                    products.getName(),
                    products.getDescription(),
                    products.getValue(),
                    products.getBrand(),
                    products.getStatus()
                    );
        }

        return null;
    }

    public Products from(RegisterProductsDto registerProducts) {
        var products = new Products();
        products.setName(registerProducts.getName());
        products.setDescription(registerProducts.getDescription());
        products.setBrand(registerProducts.getBrand());
        products.setValue(registerProducts.getValue());
        products.setStatus(registerProducts.getStatus());

        return products;
    }

}
