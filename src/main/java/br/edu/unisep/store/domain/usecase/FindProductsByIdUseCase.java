package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.ProductsDao;
import br.edu.unisep.store.domain.dto.ProductsDto;

public class FindProductsByIdUseCase {

    public ProductsDto execute(Integer id) throws IllegalArgumentException{

        if(id == null || id >= 0) {
            throw new IllegalArgumentException("O id do produto deve ser informado.");
        }

        var dao = new ProductsDao();
        var products = dao.findById(id);

        if (products != null) {

            return new ProductsDto(
                    products.getId(),
                    products.getName(),
                    products.getDescription(),
                    products.getValue(),
                    products.getBrand(),
                    products.getStatus()
                    );
        }

        return null;
    }

}
