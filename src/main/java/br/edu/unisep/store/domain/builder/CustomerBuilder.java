package br.edu.unisep.store.domain.builder;

import br.edu.unisep.store.data.entity.Customer;
import br.edu.unisep.store.domain.dto.CustomerDto;
import br.edu.unisep.store.domain.dto.RegisterCustomerDto;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

public class CustomerBuilder {

    public List<CustomerDto> from(List<Customer> customers) {
        return customers.stream().map(this::from).collect(Collectors.toList());
    }

    public CustomerDto from(Customer customer) {
        if (customer != null) {
            var today = LocalDate.now();
            var age = ChronoUnit.YEARS.between(customer.getBirthday(), today);

            return new CustomerDto(
                    customer.getId(),
                    customer.getName(),
                    customer.getEmail(),
                    customer.getBirthday(),
                    customer.getCpf(),
                    age);
        }

        return null;
    }

    public Customer from(RegisterCustomerDto registerCustomer) {
        var customer = new Customer();
        customer.setName(registerCustomer.getName());
        customer.setEmail(registerCustomer.getEmail());
        customer.setBirthday(registerCustomer.getBirthday());
        customer.setCpf(registerCustomer.getCpf());

        return customer;
    }

}