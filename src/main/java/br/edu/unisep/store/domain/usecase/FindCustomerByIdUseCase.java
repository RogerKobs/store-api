package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.CustomerDao;
import br.edu.unisep.store.domain.dto.CustomerDto;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class FindCustomerByIdUseCase {

    public CustomerDto execute(Integer id) throws IllegalArgumentException{

        if(id == null || id >= 0) {
            throw new IllegalArgumentException("O id do cliente deve ser informado.");
        }

        var dao = new CustomerDao();
        var customer = dao.findById(id);

        if (customer != null) {
            var age = ChronoUnit.YEARS.between(customer.getBirthday(), LocalDate.now());

            return new CustomerDto(
                    customer.getId(),
                    customer.getName(),
                    customer.getEmail(),
                    customer.getBirthday(),
                    customer.getCpf(),
                    age);
        }

        return null;
    }

}
