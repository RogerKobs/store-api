package br.edu.unisep.store.domain.validator;

import br.edu.unisep.store.domain.dto.RegisterProductsDto;
import org.apache.commons.lang3.Validate;

public class ProductsValidator {

    public void validate(RegisterProductsDto registerProducts)  {
        Validate.notBlank(registerProducts.getName(), "Informe o nome do produto");
        Validate.notBlank(registerProducts.getDescription(), "Informe a descrição do produto");
        Validate.notNull(registerProducts.getValue(), "Informe um valor para o produto");
        Validate.notNull(registerProducts.getBrand(), "Informe uma marca para o produto");
        Validate.notNull(registerProducts.getStatus(), "Informe o status do produto");
    }

}
