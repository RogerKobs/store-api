package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.ProductsDao;
import br.edu.unisep.store.domain.dto.ProductsDto;

import java.util.List;
import java.util.stream.Collectors;

public class FindAllProductsUseCase {

    public List<ProductsDto> execute() {
        var dao = new ProductsDao();
        var products =  dao.findAll();

        return products.stream().map(product -> {


            return new ProductsDto(
                    product.getId(),
                    product.getName(),
                    product.getDescription(),
                    product.getValue(),
                    product.getBrand(),
                    product.getStatus()
                    );
        }).collect(Collectors.toList());
    }

}
