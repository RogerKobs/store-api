package br.edu.unisep.store.domain.dto;

import lombok.Data;

@Data
public class RegisterProductsDto {

    private String name;

    private String description;

    private Double value;

    private String brand;

    private String status = "0";

}
