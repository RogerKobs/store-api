package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.CustomerDao;
import br.edu.unisep.store.domain.dto.CustomerDto;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

public class FindAllCustomersUseCase {

    public List<CustomerDto> execute() {
        var dao = new CustomerDao();
        var customers =  dao.findAll();

        var today = LocalDate.now();

        return customers.stream().map(customer -> {
            var age = ChronoUnit.YEARS.between(customer.getBirthday(), today);


            return new CustomerDto(
                    customer.getId(),
                    customer.getName(),
                    customer.getEmail(),
                    customer.getBirthday(),
                    customer.getCpf(),
                    age);
        }).collect(Collectors.toList());
    }

}
