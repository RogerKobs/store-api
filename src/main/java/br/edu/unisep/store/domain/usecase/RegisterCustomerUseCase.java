package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.CustomerDao;
import br.edu.unisep.store.data.entity.Customer;
import br.edu.unisep.store.domain.dto.RegisterCustomerDto;
import br.edu.unisep.store.domain.validator.CustomerValidator;

public class RegisterCustomerUseCase {

    public void execute(RegisterCustomerDto registerCustomer) {
        var validator = new CustomerValidator();
        validator.validate(registerCustomer);

        var customer = new Customer();
        customer.setName(registerCustomer.getName());
        customer.setEmail(registerCustomer.getEmail());
        customer.setBirthday(registerCustomer.getBirthday());
        customer.setCpf(registerCustomer.getCpf());

        var dao = new CustomerDao();
        dao.save(customer);
    }

}
